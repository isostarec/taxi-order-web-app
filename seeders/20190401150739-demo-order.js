'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('orders', [{
      status: 'In progress',
      ordered_at: '2019-04-01',
      origin: 'Rijeka, Croatia',
      destination: 'Zagreb, Croatia',
      fare_price: 53.4,
      phone_num: '+385 11 111 1111',
      email: 'sostarecivan@outlook.com',
      travel_distance: 128,
      expected_duration: 5400,
      order_number: 1
    }], {});
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.
  
      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
