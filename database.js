const Sequelize = require('sequelize');

module.exports = new Sequelize(process.env.DATABASE_URL, { dialect: 'postgres', logging: true });
//     process.env.DB_NAME || 'postgres',
//     process.env.DB_USR || 'postgres',
//     process.env.DB_PW || 'postgres',
//     {
//         host: 'localhost',
//         port: 5432,
//         dialect: 'postgres',

//         define: {
//             timestamps: false
//         },

//         pool: {
//             max: 5,
//             min: 0,
//             acquire: 30000,
//             idle: 10000
//         }
//     }

// );
