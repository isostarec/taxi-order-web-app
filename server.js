const express = require('express');
const cors = require('cors');
const graphqlHTTP = require('express-graphql');
const schema = require('./schemas/schema');
const path = require('path');

const app = express();

const PORT = process.env.PORT || 4000;

//CORS
app.use(cors());

app.use('/gql', graphqlHTTP({
    schema,
    graphiql: true
}));

app.use(express.static('public'));

app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'public', index.html))
});

app.listen(PORT, () => console.log(`Express server is listening on port ${PORT}`));