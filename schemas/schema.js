//Imports & requires
const { DateTime, EmailAddress } = require('@okgrow/graphql-scalars');
const sequelize = require('sequelize');
const db = require('../database');
const Order = require('../models/Orders');
const googleMapsClient = require('@google/maps').createClient({
    key: process.env.GOOGLE_MAPS_API_KEY,
    Promise: Promise
});
const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLList,
    GraphQLInt,
    GraphQLFloat,
    GraphQLString,
    GraphQLNonNull
} = require('graphql');

//Test db conn
db.authenticate()
    .then(() => console.log('Database is connected!'))
    .catch(err => console.log(`Error connecting to database! DETAILS: ${err} `))

//GraphQL stuff from here on

const OrderType = new GraphQLObjectType({
    name: 'Order',
    fields: () => ({
        id: { type: GraphQLInt },
        status: { type: GraphQLString },
        ordered_at: { type: GraphQLString },
        origin: { type: GraphQLString },
        destination: { type: GraphQLString },
        fare_price: { type: GraphQLFloat },
        phone_num: { type: GraphQLString },
        email: { type: EmailAddress },
        travel_distance: { type: GraphQLInt },
        expected_duration: { type: GraphQLInt },
        order_number: { type: GraphQLInt }
    })
});

const TripType = new GraphQLObjectType({
    name: 'Trip',
    fields: () => ({
        distance: { type: GraphQLString },
        duration: { type: GraphQLString },
        response: { type: GraphQLString }
    })
});

const OrderUpdateType = new GraphQLObjectType({
    name: 'OrderUpdate',
    fields: () => ({
        affected_rows: { type: GraphQLInt }
    })
});

const RootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        orders: {
            type: new GraphQLList(OrderType),
            resolve(parent, args) {
                return Order.findAll({ raw: true })
                    .then(orders => orders)
            }
        },
        order: {
            type: OrderType,
            args: {
                id: { type: GraphQLInt }
            },
            resolve(parent, args) {
                return Order.findAll({ where: { id: args.id }, raw: true })
                    .then(order => order[0]);
            }
        },
        trip: {
            type: TripType,
            args: {
                origins: { type: GraphQLString },
                destinations: { type: GraphQLString }
            },
            resolve(_, args) {
                return googleMapsClient.distanceMatrix({
                    origins: args.origins,
                    destinations: args.destinations,
                    // origins: '45.327065,14.442176',
                    // destinations: '45.311299,14.485418',
                    units: 'imperial'
                }).asPromise()
                    .then((response) => {
                        var resp_status = response.json.rows[0].elements[0].status;

                        if (resp_status != "ZERO_RESULTS") {
                            var my_distance = response.json.rows[0].elements[0].distance.value;
                            var my_duration = response.json.rows[0].elements[0].duration.value;
                        } else {
                            var my_distance = null;
                            var my_duration = null;
                        }

                        var obj = {
                            distance: my_distance,
                            duration: my_duration,
                            response: resp_status
                        }
                        return obj;
                    });
            }
        }
    }
});

// Mutations
const mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addOrder: {
            type: OrderType,
            args: {
                ordered_at: { type: new GraphQLNonNull(GraphQLString) },
                origin: { type: new GraphQLNonNull(GraphQLString) },
                destination: { type: new GraphQLNonNull(GraphQLString) },
                fare_price: { type: new GraphQLNonNull(GraphQLFloat) },
                phone_num: { type: new GraphQLNonNull(GraphQLString) },
                email: { type: EmailAddress },
                travel_distance: { type: new GraphQLNonNull(GraphQLInt) },
                expected_duration: { type: new GraphQLNonNull(GraphQLInt) },
                order_status: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parentValue, args) {
                return Order.create({
                    ordered_at: args.ordered_at,
                    origin: args.origin,
                    destination: args.destination,
                    fare_price: args.fare_price,
                    phone_num: args.phone_num,
                    email: args.email,
                    travel_distance: args.travel_distance,
                    expected_duration: args.expected_duration,
                    status: args.order_status
                });
            }
        },
        updateOrder: {
            type: OrderUpdateType,
            args: {
                id: { type: new GraphQLNonNull(GraphQLInt) },
                ordered_at: { type: GraphQLString },
                origin: { type: GraphQLString },
                destination: { type: GraphQLString },
                fare_price: { type: GraphQLFloat },
                phone_num: { type: GraphQLString },
                email: { type: EmailAddress },
                travel_distance: { type: GraphQLInt },
                expected_duration: { type: GraphQLInt },
                order_status: { type: GraphQLString }
            },
            resolve(parentValue, args) {
                return Order.update(
                    { status: args.order_status },
                    { where: { id: args.id } }
                ).then((order) => {
                    order = { affected_rows: order[0] }
                    console.log(order)
                })
                    .error((error) => error)
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation
});