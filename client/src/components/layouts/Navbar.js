import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';

export class Navbar extends Component {

    constructor() {
        super();
        this.state = {
            loggedIn: false
        }
    }

    logOff = () => {
        this.props.logOff();
        return <Redirect push to="/" />;
    }

    setActive = (e) => e.className = 'nav-item-active';

    renderLoginBtn = () => {
        if (this.props.isLoggedIn) {
            return (
                <React.Fragment>
                    <li onClick={this.logOff} style={{ position: 'absolute', right: '1em' }}>
                        <button type="button" className="btn btn-secondary">Logout</button>
                    </li>
                </React.Fragment>
            )
        }
    }

    render() {
        return (
            <div>
                <nav className="navbar navbar-expand navbar-dark bg-primary" style={{ paddingLeft: '2em', marginLeft: 0 }}>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarColor01">
                        <ul className="navbar-nav mr-auto">
                            <li onClick={this.setActive}>
                                <Link to="/" className="nav-link">Home <span className="sr-only">(current)</span></Link>
                            </li>
                            <li onClick={this.setActive} className="nav-item">
                                <Link to="/dashboard" className="nav-link">Dashboard </Link >
                            </li>
                            {this.renderLoginBtn()}
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
}

export default Navbar
