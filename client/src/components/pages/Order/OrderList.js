import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

import 'react-table/react-table.css';
import OrderDetails from './OrderDetails';
import OrdersTable from './OrdersTable';

const ORDERS_QUERY = gql`
query ordersQuery{
    orders{
        id,
        status,
        ordered_at,
        origin,
        destination,
        fare_price
    }
}
`;

export class OrderList extends Component {

    constructor() {
        super();
        this.state = {
            username: '',
            selectedOrderId: 0,
            inOrderDetals: false,
            orderStatus: {
                id: 0,
                status: ''
            }
        }
    }

    displayOrderView(obj) {
        let selectedOrderId = obj.selectedOrderId;
        let inOrderDetals = obj.inOrderDetals;

        this.setState({ selectedOrderId: selectedOrderId, inOrderDetals: inOrderDetals })
    }

    toggleInOrderDetals = () => {
        let orderDetails = this.state.inOrderDetals;
        orderDetails = !orderDetails;
        this.setState({ inOrderDetals: orderDetails })
    }

    updateOrderStatus = (orderId, orderStatus) => {
        this.setState(state => {
            let newOrderStatus = orderStatus;

            return {
                orderStatus: {
                    id: orderId,
                    status: newOrderStatus
                }
            }
        })
    }

    greet(name) {
        let firstLetter = name.charAt(0).toUpperCase();
        let restOfName = name.slice(1);
        return firstLetter + restOfName + '! 🤗';
    }

    render() {
        var query = (
            <Query query={ORDERS_QUERY}>
                {
                    ({ loading, error, data }) => {
                        let isLoading = false;

                        const columns = [{
                            Header: 'Id',
                            accessor: 'id'
                        }, {
                            Header: 'Status',
                            accessor: 'status'
                        }, {
                            id: 'ordered_at',
                            Header: 'Time ordered',
                            accessor: d => `${new Date(parseInt(d.ordered_at))}`
                        }, {
                            Header: 'Departure',
                            accessor: 'origin'
                        }, {
                            Header: 'Destination',
                            accessor: 'destination'
                        }, {
                            id: 'fare_price',
                            Header: 'Price',
                            accessor: d => `$ ${d.fare_price}`
                        }, {
                            id: 'order_details',
                            Header: 'Order Details',
                            accessor: d => (
                                <React.Fragment>
                                    <div >
                                        <button id="orderSelectBtn" className="btn-sm btn-success">Order Details</button>
                                    </div>
                                </React.Fragment>
                            )
                        }]

                        // id: 'id',
                        // status: 'status',
                        // ordered_at: 'Time ordered',
                        // origin: 'Departure',
                        // destination: 'Destination',
                        // fare_price: 'Price'

                        if (loading) return isLoading;
                        return (
                            <React.Fragment>
                                <div className="container" style={{ textAlign: 'center', marginTop: '1em' }}>
                                    <h1>Welcome, {this.greet(this.props.name)}</h1>
                                    <hr></hr>
                                    <h1>Order list</h1>
                                    <OrdersTable
                                        orderData={data.orders}
                                        tableColumns={columns}
                                        selectedOrder={this.displayOrderView.bind(this)}
                                        orderStatus={this.state.orderStatus}
                                    />
                                </div>
                            </React.Fragment>
                        )
                    }
                }
            </Query>
        )
        if (this.state.selectedOrderId > 0 && this.state.inOrderDetals === true) {
            return (
                <OrderDetails
                    orderData={this.state.selectedOrderId}
                    inOrderDetalsTgl={this.toggleInOrderDetals.bind(this)}
                    updateOrderStatus={this.updateOrderStatus}
                />
            )
        } else {
            return (
                <React.Fragment>
                    {query}
                </React.Fragment>
            )
        }
    }
}

export default OrderList
