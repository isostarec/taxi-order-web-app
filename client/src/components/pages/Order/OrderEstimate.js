import React, { Component } from 'react'

export class OrderEstimate extends Component {
    // state: IState;

    constructor() {
        super();
        this.state = {
            location: {
                locFrom: '',
                locTo: ''
            }
        }
    };

    styleDivBox = () => {
        return {
            backgroundColor: '',
            fontSize: '4em'
        }
    }

    styleFareAmount = () => {
        return {
            textAlign: 'left',
            fontSize: '1em'
        }
    }

    styleFareAmountFailed = () => {
        return {
            width: '250px',
            marginTop: '0.2em'
        }
    }

    placeholderStyle = () => {
        return {
            paddingTop: '15px'
        }
    }

    gotZeroResults(data) {
        if (data) {
            if (data.trip.response === "ZERO_RESULTS") {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    render() {
        if (!this.gotZeroResults(this.props.reqData)) {
            if (!this.props.isLoading) {
                return (
                    <React.Fragment>
                        <h3>Estimated price:</h3>
                        <p style={this.styleFareAmount()}>~${this.props.calculatedPrice}</p>
                    </React.Fragment>
                );
            } else {
                return (
                    <h1>Calculating...</h1>
                );
            }
        } else {
            return (<p style={this.styleFareAmountFailed()}>Your trip could not be calculated. Please try again later!</p>);
        }
    }
}


export default OrderEstimate
