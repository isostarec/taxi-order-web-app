import React, { Component } from 'react'
import ReactTable from 'react-table';

export class OrdersTable extends Component {

    constructor(props) {
        super();
        this.state = {
            orderDetails: [],
            selectedOrder: {},
            tableColumns: [],
            update: 0
        }
    }

    componentWillReceiveProps(props) {
        this.setState({ orderDetails: props.orderData })
    }

    componentWillMount() {
        let newOrderData = this.props.orderData;

        this.setState(state => {
            return {
                orderDetails: newOrderData
            }
        }, () => {
            this.updateOrderStatus()
                .then(data => {
                    this.setState({ orderDetails: data });

                })
        })
    }

    updateOrderStatus = () => {
        let returnPromise = (data) => new Promise((resolve, reject) => {
            if (!data) {
                reject('An error occured updating order status!');
            } else {
                resolve(data);
            }
        });

        let idToUpdate = this.props.orderStatus.id;
        let statusToUpdate = this.props.orderStatus.status;


        if (idToUpdate > 0) {
            let orderDetails = this.state.orderDetails;
            this.setState(state => {
                let empty = [];
                return {
                    orderDetails: empty
                }
            }, () => this.setState({ orderDetails: orderDetails }))

            let data = this.state.orderDetails;
            let newData = data;
            newData.forEach((e) => {
                if (e.id === idToUpdate) {
                    e.status = statusToUpdate
                }
            })

            return returnPromise(newData)
        } else {
            return returnPromise(false)
        }
    }

    onRowClick = (state, rowInfo, column, instance) => {
        return {
            onClick: (e) => {
                if (e.target.id === "orderSelectBtn") {
                    this.props.selectedOrder({ selectedOrderId: rowInfo.row.id, inOrderDetals: true });
                }
            }
        }
    }

    render() {
        return (
            <div>
                <ReactTable
                    data={this.state.orderDetails}
                    columns={this.props.tableColumns}
                    getTrProps={this.onRowClick}
                    defaultPageSize={5}
                    selectedOrder={this.state.selectedOrder}
                />
            </div >
        )
    }
}

export default OrdersTable
