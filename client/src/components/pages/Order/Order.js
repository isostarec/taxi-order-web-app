/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

import './Order.css';

import OrderForm from './OrderForm';
import OrderEstimate from './OrderEstimate';

const TRIP_QUERY = gql`
   query TripQuery($origins: String, $destinations: String){
    trip(origins:$origins, destinations:$destinations) {
      distance,
      duration,
      response
    }
  }
`;

class Order extends Component {

    initialLocationState = {
        locFrom: '',
        locTo: ''
    }

    constructor() {
        super();
        this.state = {
            fare: 0.0,
            location: {
                locFrom: '',
                locTo: ''
            },
            tripDetails: {
                distance: '',
                duration: ''
            },
            orderFormState: {
                pickupLocFrom: '',
                pickupLocFromName: '',
                pickupLocTo: '',
                pickupLocToName: '',
                pickupTime: new Date(),
                pickupPhoneNum: '',
                pickupEmail: '',
                fare_distance: 0,
                fare_duration: 0,
                fare: 0.0
            }
        }
    }

    orderStyle = () => {
        return {
            margin: '2em 2em 0 2em'
        }
    }

    styleDivBox = () => {
        return {
            backgroundColor: '',
            fontSize: '4em'
        }
    }

    styleFareAmount = () => {
        return {
            textAlign: 'left',
            fontSize: '1em'
        }
    }

    placeholderStyle = () => {
        return {
            paddingTop: '15px'
        }
    }

    componentWillReceiveProps = (newProps) => {
        if (this.state.location !== newProps.orderLocation) {
            let location = [];
            this.setLocationState(newProps, location);
        } else {
            this.setLocationState(newProps);
        }

    }

    componentDidMount() {
        this.setState({ location: this.initialLocationState })
    }

    onUpdateFare = (newLocation) => this.setState({ location: newLocation });

    onUpdateTripDetails = (newTripData) => this.setState({ tripDetails: newTripData, });

    updateFormState(newFormData) {
        this.setState({ orderFormState: newFormData });
    }

    setLocationState(newProps, location) {
        this.setState(state => {
            location = newProps.orderLocation;
            return {
                location: location
            };
        });
    }

    calcEstimate(newTripData) {
        if (newTripData.trip.response !== "OK") {
            return false
        } else {
            let milesInMeters = 0.000621371192;
            let price = 0.0;

            let distance = newTripData.trip.distance * milesInMeters;
            if (distance <= 3) {
                price = 3;
            } else if (distance > 3) {
                price = distance;
            }
            return price.toFixed(3);
        }
    }

    checkLocState() {
        if (!this.state.location) {
            this.setState(state => {
                let location = this.initialLocationState;
                return {
                    location: location
                }
            });
        }
    }

    validateLocation(loc) {
        this.checkLocState();
        //Check if loc obj exists, evade undefined error
        if (loc) {
            if (loc.locFrom.length > 0 && loc.locTo.length > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    render() {
        if (this.validateLocation(this.state.location)) {
            let origins = this.state.location.locFrom;
            let destinations = this.state.location.locTo;

            var query = (
                <Query query={TRIP_QUERY} variables={{ origins, destinations }}>
                    {
                        ({ loading, error, data }) => {
                            let isLoading;
                            if (loading) {
                                return isLoading = true;
                            } else {
                                isLoading = false;
                            }
                            return (
                                <React.Fragment>
                                    <div className='container' style={{ margin: '0 2em 0 2em !important', paddingLeft: '3em !important' }}>
                                        <div className='row' style={this.orderStyle()}>
                                            <div className='col-12 col-lg-6 col-md-6 col-sm-6' style={{ marginBottom: '3em' }}>
                                                <OrderForm
                                                    globalFormState={this.state.orderFormState}
                                                    pushFormStateToParent={this.updateFormState.bind(this)}
                                                    changeFare={this.onUpdateFare.bind(this)}
                                                    farePrice={this.calcEstimate(data)}
                                                    tripDetails={data.trip}
                                                />
                                            </div>
                                            <div style={{ margin: '1em 0 0 0' }}>
                                                <OrderEstimate
                                                    isLoading={isLoading}
                                                    reqData={data}
                                                    orderLocation={this.state.location}
                                                    calculatedPrice={this.calcEstimate(data)}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </React.Fragment >
                            );
                        }
                    }
                </Query>
            )
            return (
                <React.Fragment>
                    {query}
                </React.Fragment>
            );
        } else {
            return (
                <div style={{ margin: '0 2em 0 2em !important', paddingLeft: '3em !important' }}>
                    <div className='row' style={this.orderStyle()}>
                        <div className='col-12 col-lg-6 col-md-6 col-sm-6 mx-auto rounded' style={{ marginBottom: '3em !important', marginRight: '5em !important', width: '100%', backgroundColor: 'rgb(241, 244, 255' }}>
                            <OrderForm
                                globalFormState={this.state.orderFormState}
                                pushFormStateToParent={this.updateFormState.bind(this)}
                                changeFare={this.onUpdateFare.bind(this)}
                                tripDetails={this.state.tripDetails}
                            />
                        </div>
                    </div>
                </div>
            )
        }
    }
}

export default Order;
