/* eslint-disable no-unused-vars */
import React, { Component } from 'react';
import Geosuggest from 'react-geosuggest';
import Datepicker from 'react-datepicker';
import { Mutation } from "react-apollo";
import gql from 'graphql-tag';
import 'react-datepicker/dist/react-datepicker.css'
import './OrderForm.css';

const GOOGLE_MAPS_API_KEY = process.env.GOOGLE_MAPS_API_KEY;
const SUBMIT_QUERY = gql`
mutation orderAddM($ordered_at: String!, $origin: String!, $destination: String!, $fare_price: Float!, $phone_num: String!, $email: EmailAddress, $travel_distance: Int!, $expected_duration: Int!, $order_status: String!){
    addOrder(ordered_at: $ordered_at, origin: $origin, destination: $destination, fare_price: $fare_price, phone_num: $phone_num, email: $email, travel_distance: $travel_distance, expected_duration: $expected_duration, order_status: $order_status){
        id
    }
  }
`;

class OrderForm extends Component {

    constructor() {
        super();
        this.state = {
            pickupLocFrom: '',
            pickupLocTo: '',
            pickupLocFromName: '',
            pickupLocToName: '',
            pickupTime: new Date(),
            pickupPhoneNum: '',
            pickupEmail: '',
            fare_distance: 0,
            fare_duration: 0,
            fare: 0.0,
            charLen: 0,
        }
    }

    geosuggestStyle = {
        'suggests': {
        }
    }

    componentDidMount = () => this.setState(this.props.globalFormState)

    onBlur = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({ [name]: value });
        this.props.pushFormStateToParent(this.state);
    }

    onFromSelect(destination) {
        if (destination) {
            this.setState({
                pickupLocFrom: `${destination.location.lat},${destination.location.lng}`,
                pickupLocFromName: `${destination.label}`
            });
            return this.onFareChange();
        }
    }

    onToSelect(destination) {
        if (destination) {
            this.setState({
                pickupLocTo: `${destination.location.lat},${destination.location.lng}`,
                pickupLocToName: `${destination.label}`
            });
            return this.onFareChange();
        }
    };

    onFareChange() {
        let location = {
            locFrom: this.state.pickupLocFrom,
            locTo: this.state.pickupLocTo
        };
        this.props.pushFormStateToParent(this.state);
        this.props.changeFare(location);
    };

    onPickupTimeChange = (time) => {
        this.setState({ pickupTime: time });
        this.props.pushFormStateToParent(this.state);
    }

    validateForm() {
        if (this.state.pickupPhoneNum.length > 0 && this.state.pickupLocFromName.length > 0 && this.state.pickupLocToName.length > 0) {
            return true;
        } else {
            return false;
            // <h3>Please fill in Phone number and arrival/departure destinations.</h3>
        }

    }

    render() {
        return this.submitOrder();
    }
    submitOrder = () => {
        const expectedFare = this.props.expectedFare;

        return (
            <div className="orderForm" autoComplete="none">
                <h1 style={{ textAlign: 'center' }}>Book a ride <span role="img" aria-label="red-car">🚗</span></h1>
                <Mutation mutation={SUBMIT_QUERY}>
                    {(addOrder, { data }) => (
                        <form
                            onSubmit={e => {
                                e.preventDefault();
                                addOrder({
                                    variables: {
                                        ordered_at: this.state.pickupTime,
                                        origin: this.state.pickupLocFromName,
                                        destination: this.state.pickupLocToName,
                                        fare_price: parseFloat(this.props.farePrice),
                                        phone_num: this.state.pickupPhoneNum,
                                        email: this.state.pickupEmail,
                                        travel_distance: parseInt(this.props.tripDetails.distance),
                                        expected_duration: parseInt(this.props.tripDetails.duration),
                                        order_status: 'Requested'
                                    }
                                });
                            }}
                        >
                            <div className="form-group">
                                <input
                                    name="pickupEmail"
                                    defaultValue={this.props.globalFormState.pickupEmail}
                                    type="email"
                                    id="contact-email"
                                    placeholder="Your E-mail"
                                    className="form-control"
                                    onBlur={e => this.onBlur(e)}
                                />
                            </div>
                            <div className="form-group">
                                <input
                                    name="pickupPhoneNum"
                                    defaultValue={this.props.globalFormState.pickupPhoneNum}
                                    type="text"
                                    id="contact-num"
                                    placeholder="Phone number"
                                    className="form-control"
                                    onBlur={e => this.onBlur(e)}
                                />
                            </div>
                            <div className="form-group">
                                <Geosuggest
                                    initialValue={this.props.globalFormState.pickupLocFromName}
                                    ref={el => this._geoSuggest = el}
                                    id="loc-from"
                                    placeholder="From"
                                    style={this.geosuggestStyle}
                                    inputClassName="form-control"
                                    onSuggestSelect={this.onFromSelect.bind(this)}
                                />
                            </div>
                            <div className="form-group">
                                <Geosuggest
                                    initialValue={this.props.globalFormState.pickupLocToName}
                                    ref={el => this._geoSuggest = el}
                                    id="loc-to"
                                    placeholder="To"
                                    style={this.geosuggestStyle}
                                    inputClassName="form-control"
                                    onSuggestSelect={this.onToSelect.bind(this)}
                                />

                            </div>
                            <div className="form-group">
                                <Datepicker
                                    style={{ width: '100%' }}
                                    className="form-control"
                                    selected={this.state.pickupTime}
                                    onChange={this.onPickupTimeChange}
                                    showTimeSelect
                                    timeFormat="HH:mm"
                                    timeIntervals={1}
                                    dateFormat="MMMM d, yyyy h:mm aa"
                                    timeCaption="time"
                                />
                            </div>
                            <button type="submit"
                                id="Submit"
                                className="btn btn-primary"
                                disabled={!this.validateForm()}
                                style={{ width: '100%' }}>Order</button>
                        </form>
                    )}
                </Mutation>
            </div>
        )
    }
}

export default OrderForm;