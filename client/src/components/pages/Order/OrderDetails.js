import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import OrderStatusDropdown from './OrderStatusDropdown';

import './OrderDetails.css';

const ORDER_QUERY = gql`
query orderQuery($id: Int){
    order(id: $id){
        id,
        status,
        ordered_at,
        origin,
        destination,
        fare_price,
        phone_num,
        email,
        travel_distance,
        expected_duration
    }
}
`;
export class OrderDetails extends Component {

    constructor() {
        super();
        this.state = {
            orderData: {},
            orderStatus: {
                requested: 'Requested',
                declined: 'Declined',
                inProgress: 'In progress',
                done: 'Done'
            }
        }
    }

    newOrderStatus = (orderId, orderStatus) => {
        this.props.updateOrderStatus(orderId, orderStatus);
    }

    render() {
        if (this.props.orderData > 0) {
            let id = this.props.orderData;
            var order = (
                <Query query={ORDER_QUERY} variables={{ id }} onCompleted={this.returnOrderDetails}>
                    {
                        ({ loading, error, data }) => {

                            if (!data.order) {
                                return <p>No data</p>
                            } else {
                                let orderId = data.order.id;
                                let orderStatus = data.order.status;
                                let myData = data.order;
                                let travelDistance = data.order.travel_distance / 1000;
                                let tripDuration = data.order.expected_duration / 60 / 60;
                                let orderTime = new Date(parseInt(data.order.ordered_at));
                                orderTime = orderTime.toDateString();

                                return (

                                    < React.Fragment >
                                        <div className="container" >
                                            <div style={{ marginBottom: '1em' }}>
                                                <OrderStatusDropdown
                                                    orderId={orderId}
                                                    orderStatus={orderStatus}
                                                    newOrderStatus={this.newOrderStatus}
                                                />
                                            </div>
                                            <ul style={{ listStyle: 'none' }}>
                                                <li>
                                                    Order time: {orderTime}
                                                </li>
                                                <li>
                                                    Order origin: {myData.origin}
                                                </li>
                                                <li>
                                                    Order destination: {myData.destination}
                                                </li>
                                                <li>
                                                    Fare price: ${myData.fare_price}
                                                </li>
                                                <li>
                                                    Phone number: {myData.phone_num}
                                                </li>
                                                <li>
                                                    Email: {myData.email}
                                                </li>
                                                <li>
                                                    Travel distance (kilometers): {travelDistance}
                                                </li>
                                                <li>
                                                    Expected trip duration: {tripDuration.toFixed(2)} hours
                                            </li>
                                            </ul>
                                        </div >
                                    </React.Fragment>
                                )

                            }
                        }
                    }
                </Query>
            )
        }

        return (
            <div className="container" style={{ textAlign: 'center', marginTop: '1em' }}>
                <div className="row" style={{ textAlign: 'center' }}>
                    <button
                        type="button"
                        onClick={this.props.inOrderDetalsTgl}
                        className="btn btn-link col-lg-2" >
                        Back to order list
                </button>
                    <h1 className="col-lg-9">Order details for order no. {this.props.orderData}</h1>
                </div>
                <hr></hr>
                {order}
            </div>
        )
    }
}

export default OrderDetails
