import React, { Component } from 'react'
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import Select from 'react-select';

const ORDER_STATUS_MUTATION = gql`
mutation updateOrderM($id: Int!, $order_status: String) {
    updateOrder(id: $id, order_status: $order_status) {
      affected_rows
    }
  }
`;


const orderStatuses = [
    { value: 'Requested', label: 'Requested' },
    { value: 'Declined', label: 'Declined' },
    { value: 'In progress', label: 'In progress' },
    { value: 'Done', label: 'Done' }
];

export class OrderStatusDropdown extends Component {

    dropdownMenu = (id, selectedStatus) => {
        return (
            <Mutation mutation={ORDER_STATUS_MUTATION}>
                {(updateOrder, { data, loading, error }) => (
                    <div>
                        Order status:
                    <Select
                            className={'selectStatus'}
                            onChange={(e) => {
                                updateOrder({ variables: { id: this.props.orderId, order_status: e.value } })
                                this.props.newOrderStatus(this.props.orderId, e.value)
                            }}
                            placeholder={this.props.orderStatus}
                            options={orderStatuses}
                        />
                    </div>
                )
                }
            </Mutation>
        )

    }

    render() {
        return (
            this.dropdownMenu()
        )
    }
}

export default OrderStatusDropdown
