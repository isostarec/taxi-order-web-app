import React, { Component } from 'react';
import OrderList from '../Order/OrderList';

export class Dashboard extends Component {

    constructor() {
        super();
        this.state = {
            loggedIn: false,
            username: '',
            password: '',
            name: 'ivan',
            error: '',
            loginSuccess: ''
        };

        this.handlePassChange = this.handlePassChange.bind(this);
        this.handleUserChange = this.handleUserChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.dismissError = this.dismissError.bind(this);
    }

    dismissError() {
        this.setState({ error: '' });
    }
    styleLoginMessage() {
        if (this.state.loggedIn) {
            return 'alert alert-primary';
        } else {
            return 'alert alert-danger';
        }
    }


    validateLoginData() {
        if (this.state.username === 'ivan' && this.state.password === 'testapp74') {
            this.props.logIn();
            this.setState({ loggedIn: true, username: this.state.username });
        } else {
            return this.setState({ error: 'Login credentials incorrect!' });
        }
    }

    handleSubmit(e) {
        e.preventDefault();

        if (!this.state.username) {
            return this.setState({ error: 'Username is required' });
        }

        if (!this.state.password) {
            return this.setState({ error: 'Password is required' });
        }

        this.validateLoginData();

        return this.setState({ error: '' });
    }

    handleUserChange(e) {
        this.setState({
            username: e.target.value,
        });
    };

    handlePassChange(e) {
        this.setState({
            password: e.target.value,
        });
    }

    render() {
        let login = (
            <div className="container Login">
                <h1 style={{ textAlign: "center", marginTop: '1em' }}>Login</h1>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    {
                        this.state.error && this.styleLoginMessage &&
                        <div className='alert alert-danger' role="alert">
                            {this.state.error}
                        </div>
                    }
                    <div className="form-group">
                        <label className="grid-item"></label>
                        <input className="form-control" type="text" placeholder="username" value={this.state.username} onChange={this.handleUserChange} />
                    </div>

                    <div className="form-group">
                        <label htmlFor="pass-to" className="grid-item"></label>
                        <input id="pass-field" className="form-control" type="password" placeholder="password" value={this.state.password} onChange={this.handlePassChange} />
                    </div>

                    <button style={{ width: '100%' }} className="btn btn-primary" type="submit" value="Log In" data-test="submit">Login</button>
                </form>
            </div>
        )

        if (this.props.isLoggedIn) {
            return (
                <OrderList
                    username={this.state.username}
                    name={this.state.name} />
            )
        } else {
            return <React.Fragment>{login}</React.Fragment>
        }
    }
}

export default Dashboard
