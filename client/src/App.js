import React, { Component } from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Order from './components/pages/Order/Order';
import Dashboard from './components/pages/Dashboard/Dashboard';
import Navbar from './components/layouts/Navbar';
import './App.css';

// const client = new ApolloClient({
//   uri: 'http://localhost:4000/gql'
// });

const client = new ApolloClient({
  uri: '/gql'
});
class App extends Component {

  constructor() {
    super();
    this.state = {
      loggedIn: false
    }
  }

  logOff = () => this.setState({ loggedIn: false });
  logIn = () => this.setState({ loggedIn: true });

  render() {
    return (
      <ApolloProvider client={client}>
        <Router>
          <div className="App">
            <Navbar isLoggedIn={this.state.loggedIn} logOff={this.logOff} />
            <Route exact path="/" render={props => (
              <React.Fragment>
                <Order />
              </React.Fragment>
            )} />
            <Route path="/dashboard" render={props => <Dashboard logIn={this.logIn.bind(this)} isLoggedIn={this.state.loggedIn} />} />
          </div>
        </Router>
      </ApolloProvider>
    );
  }
}

export default App;
