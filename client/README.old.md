# Full-Stack Javascript Developer Coding Challenge

## Table of Content

- [Getting Started](#markdown-header-getting-started)
  - [Prerequisites](#markdown-header-prerequisites)
  - [Setup](#markdown-header-setup)
- [Challenge Description](#markdown-header-challenge-description)
  - [Front Office](#markdown-header-prerequisites)
  - [Back Office - Dispatcher dashboard](#markdown-header-back-office-dispatcher-dashboard)
  - [Pricing model](#markdown-header-pricing-model)
  - [Implementation requirements](#markdown-header-implementation-requirements)
  - [Deployment](#markdown-header-deployment)
- [Challenge Bonuses](#markdown-header-challenge-bonuses)
- [Evaluation](#markdown-header-Evaluation)
- [Submission Guidelines](#markdown-header-submission-guidelines)

## Getting Started

### Prerequisites

- [Git](http://git-scm.com/)
- [Node.js](https://nodejs.org/)
- [React](https://reactjs.org/)
- [Google's Distance Matrix Service](https://developers.google.com/maps/documentation/javascript/distancematrix)
- [MySQL](https://www.mysql.com/) or any other relational database
- [GraphQL](https://graphql.org/) server with Node.js ORM framework (Sequelize, Prisma or equivalent ORM)

### Setup

- [Fork](https://bitbucket.org/iolap/iolap-ory-fullstack-challenge/fork) or clone this repository: [iolap/iolap-ory-fullstack-challenge](https://bitbucket.org/iolap/iolap-ory-fullstack-challenge/)
- Setup your working environment
- Generate your backend Node.js and React projects ([Create React App](https://github.com/facebook/create-react-app))

## Challenge Description

The application serves the purpose of **ordering a taxi online**.

### Front Office

Guest user (client) opens a homepage and is represented with a form:

- Pickup location (origin address)
- Destination address
- Time and Date of pickup
- Phone number
- Email address (optional)
- Order button

Expected fare price must be displayed to the user before the actual order happens.

### Back Office - Dispatcher dashboard

Dispatcher should be able to login to the dashboard with his credentials.
On the dashboard, list of all orders should be shown in form of a table. Expected table columns are:

- Order ID
- Order Status
- Time and Date
- Origin
- Destination
- Fare price
- A button for displaying a detailed view of the order

In a detailed view of the order, dispatcher should be able to see all the information regarding that order:

- All the info in the table mentioned above
- Phone number
- Email address (if submitted)
- Distance between origin and destination points (Provided by [Google's Distance Matrix Service](https://developers.google.com/maps/documentation/javascript/distancematrix))
- Expected duration of the trip (Provided by [Google's Distance Matrix Service](https://developers.google.com/maps/documentation/javascript/distancematrix))

Additionally, _Order Status_ should be changeable by dispatcher. Possible statuses are:

- Requested
- Declined
- In progress
- Done

### Pricing model

Prices will be calculated based on the distance provided by [Google's Distance Matrix Service](https://developers.google.com/maps/documentation/javascript/distancematrix).

If the distance **is 3 miles or less** (_base distance_), fare price is **fixed to \$3** (_base price_). If the distance **is greater then 3 miles**, every _additional mile_ will cost **\$1 more**.

### Implementation requirements

- Frontend side (both back office and front office) should be implemented with [React](https://reactjs.org/) (you are encouraged to use additional libraries such as [Redux](https://redux.js.org/))
- Backend side should be using [GraphQL server](https://graphql.org/) with one of the Node.js ORMs ([Sequelize](http://docs.sequelizejs.com/), [Prisma](https://www.prisma.io/) or equivalent).
- Database should be a [MySQL](https://www.mysql.com/) or any other relational database.

### Deployment

Final version of your application should be deployed, visible and usable. You are free to use AWS, Azure or Heroku.

## Challenge Bonuses

Optional features:

- Test coverage
- Linting
- Error handling and validations
- Responsive front office UI
- Ability for dispatcher to change _base distance_, _base price_ and _additional cost_.
- Sending Email with order details to a user (if email was provided)
- Solving N+1 problems in GraphQL
- Whatever interesting you'd like to do

## Evaluation

Levels of functionality:

- **basic** - items mentioned in _Challenge Description_
- **above average** - some optional features mentioned in _Challenge Bonuses_
- **exceptional** - all of the above + CI/CD on AWS, Azure or Heroku

Application should support only the latest version of [Google Chrome](https://www.google.com/chrome/). Supporting other browsers is **not** required.

Our goal is to find answers to these questions:

- Do you understand the JavaScript language and more in general web technologies?
- Do you understand the chosen stack, framework, query language in general?
- Can you design interfaces that are clear and easy to use?
- Can you judge which library/framework is the best fit for a job and use it correctly?
- Do you master your working environment?

## Submission Guidelines

Please let your iOLAP contact know when you're finished and provide:

- URL to your repository (it has to be publicly visible)
- URL to a deployed application
- Dispatcher user credentials for your deployed application
