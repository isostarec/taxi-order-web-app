const Sequelize = require('sequelize');
const db = require('../database');

const Order = db.define('order', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    status: {
        type: Sequelize.STRING
    },
    ordered_at: {
        type: Sequelize.DATE
    },
    origin: {
        type: Sequelize.STRING
    },
    destination: {
        type: Sequelize.STRING
    },
    fare_price: {
        type: Sequelize.NUMBER
    },
    phone_num: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    travel_distance: {
        type: Sequelize.INTEGER
    },
    expected_duration: {
        type: Sequelize.INTEGER
    },
    order_number: Sequelize.INTEGER
})

module.exports = Order;