#Taxi order app

This app is made using React, GraphQL, Apollo and PostgreSQL

##Development environment setup instructions

Run this command in the root app folder (./) and in the ./client folder to install required dependencies.

```bash
npm i
```

##Running the app

```bash
npm run dev
```

##License
[MIT](https://choosealicense.com/licenses/mit/)