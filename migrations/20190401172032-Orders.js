'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.createTable('orders', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      status: Sequelize.STRING,
      ordered_at: Sequelize.DATE,
      origin: Sequelize.STRING,
      destination: Sequelize.STRING,
      fare_price: Sequelize.FLOAT,
      phone_num: Sequelize.STRING,
      email: Sequelize.STRING,
      travel_distance: Sequelize.INTEGER,
      expected_duration: Sequelize.INTEGER,
      order_number: Sequelize.INTEGER,
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.dropTable('orders');
  }
};
